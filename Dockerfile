FROM odoo:11

USER odoo
COPY --chown=odoo:odoo addons /mnt/addons
COPY --chown=odoo:odoo ./config/odoo.conf /etc/odoo/odoo.conf


USER root
# RUN echo "#!/bin/bash\n\$@" > /usr/bin/sudo
# RUN chmod +x /usr/bin/sudo
RUN apt-get install python3-pip -y
RUN chown -R root:root /usr/lib/python3/dist-packages/odoo/addons
RUN chmod 755 /usr/lib/python3/dist-packages/odoo/addons


