{
    'name': 'Auto Printer',
    'version': '1.1',
    'category': 'Auto Printer',
    'summary': 'Auto Printer',
    'description': """
            Auto Printer
    """,
    'depends': ['web'],
    'data': [
        'views/report_paperformat_views.xml'
    ],
    'demo': [
    ],
}
