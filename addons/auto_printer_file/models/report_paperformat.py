from odoo import api, fields, models, _


class report_paperformat(models.Model):
    _inherit = "report.paperformat"

    printer = fields.Char(
        string="Printer",
    )
